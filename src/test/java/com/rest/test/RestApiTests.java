package com.rest.test;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.qameta.allure.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

/**
 * Unit test for RestApiTest.
 */
public class RestApiTests
{
    /**
     * Basic Test example
     */
    @Test
    @Description("Test Description: This test will fail")
    public void thisTestWillFail()
    {
        assertTrue( false );
    }
    /**
     * Basic RestAssured example
     */    
    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Description("Test Description")
    @Story("Story that the test belongs to")
    @Tag("API")
    public void googleTest() {
        Response response = given()
                .filter(new AllureRestAssured())
                .when()
                .get("https://www.google.com/").then()
                .extract().response();
        Assertions.assertEquals(response.statusCode(), 200);
    }
}

